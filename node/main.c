/******************************************************************************
*  Nano-RK, a real-time operating system for sensor networks.
*  Copyright (C) 2007, Real-Time and Multimedia Lab, Carnegie Mellon University
*  All rights reserved.
*
*  This is the Open Source Version of Nano-RK included as part of a Dual
*  Licensing Model. If you are unsure which license to use please refer to:
*  http://www.nanork.org/nano-RK/wiki/Licensing
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, version 2.0 of the License.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/
#include <nrk.h>
#include <include.h>
#include <ulib.h>
#include <stdio.h>
#include <avr/sleep.h>
#include <hal.h>
#include <bmac.h>
#include <nrk_error.h>
#include <nrk_driver_list.h>
#include <nrk_driver.h>
#include <ff_basic_sensor.h>
#define MAC_ADDR	0x0002
nrk_task_type NODE_TASK;
NRK_STK node_task_stack[NRK_APP_STACKSIZE];
void node_task (void);
uint16_t tx_node_id,rx_node_id;
void nrk_create_taskset ();
uint8_t node_buf[RF_MAX_PAYLOAD_SIZE];
uint8_t rx_buf[RF_MAX_PAYLOAD_SIZE];
uint8_t tx_buf[RF_MAX_PAYLOAD_SIZE];
void nrk_register_drivers();
int main ()
{
  uint16_t div;
  nrk_setup_ports ();
  nrk_setup_uart (UART_BAUDRATE_115K2);
  nrk_init ();
  nrk_led_clr (0);
  nrk_led_clr (1);
  nrk_led_clr (2);
  nrk_led_clr (3);
  nrk_time_set (0, 0);
  bmac_task_config ();
  nrk_register_drivers();
  nrk_create_taskset ();
  nrk_start ();
  return 0;
}
void node_task ()
{
    uint8_t i, j,k, len, end_flag;
    uint8_t audio_count, pir_count,time_out,negative_flag, to_flag;
    uint8_t fd;
    int8_t rssi, val;
    uint16_t motion=5, audio=5;
    uint8_t *local_buf = NULL;
    uint8_t *localrx_buf = NULL;
    uint8_t routing_array[11]={0,0,0,0,0,0,0,0,0,0,0};
    //Opening sensor driver
    printf ("node_task PID=%d\r\n", nrk_get_pid ());
    fd=nrk_open(FIREFLY_3_SENSOR_BASIC,READ);
    if(fd==NRK_ERROR) 
        nrk_kprintf(PSTR("Failed to open sensor driver\r\n"));
  
    bmac_init (12);
    bmac_rx_pkt_set_buffer (rx_buf, RF_MAX_PAYLOAD_SIZE);
    nrk_time_t start_time, end_time;
    audio_count = 0;
    pir_count = 0;
    while(1)
    {
        //Initialisation
        tx_node_id = -1;
        rx_node_id = -1;
        end_flag =0;
        audio_count = 0;
        pir_count = 0;
        to_flag = 0;
        time_out = 0;
        negative_flag = 0;
    
        //Waiting for Rout Info
        nrk_led_set(RED_LED);
        nrk_led_set(GREEN_LED);
        nrk_led_set(ORANGE_LED);
        nrk_led_set(BLUE_LED);
        do
        {
            printf(PSTR("Waiting for Alert\r\n"));
            local_buf = NULL;
            if(!bmac_rx_pkt_ready())
            {
       	        val = bmac_wait_until_rx_pkt ();
            }
            local_buf = bmac_rx_pkt_get (&len, &rssi);
            bmac_rx_pkt_release ();
            printf("Received Alert : %s \r\n",local_buf);
            if(local_buf[0] == 'H' || local_buf[0] == 'T')
            { 
                i = (local_buf[1] - '0') + 2; 
                j = local_buf[i] - '0';
            }
        }while((local_buf[0] != 'H' || j != MAC_ADDR) && (local_buf[0] != 'T' || j!=MAC_ADDR)); 


        nrk_led_clr(RED_LED);
        nrk_led_clr(GREEN_LED);
        nrk_led_clr(ORANGE_LED);
        nrk_led_clr(BLUE_LED);
         
        if(local_buf[0] == 'T')
            to_flag = 1;
        //Analysing Rout info
        //Finding tx_node
        tx_node_id = local_buf[i-1] - '0';
        
        //Finding rx_node
        if(local_buf[i+1]!='F')
        {
            rx_node_id = local_buf[i+1] - '0';
            local_buf[1] = (i-1) + '0';
        }
        else
        {
            rx_node_id = MAC_ADDR;
            end_flag = 1;
        }


        if(local_buf[0] == 'H' || local_buf[0] == 'T')
        {
            routing_array[0] = local_buf[0];
            for(k=1; local_buf[k]!='F'; k++)
            {
                routing_array[k] = local_buf[k] - '0';
            }
            routing_array[k] = local_buf[k];
        }

        if(end_flag!=1)
            printf("Node %d :: transmit to %d , receive from %d\r\n", MAC_ADDR, tx_node_id, rx_node_id);
        else
            printf("End-Node %d :: transmit to %d\r\n", MAC_ADDR, tx_node_id);
        //Send or Wait
        if(end_flag == 1)
        {
            //Sample sensors 
            nrk_time_get(&start_time);
            while (1)
            {
                val=nrk_set_status(fd,SENSOR_SELECT,MOTION);
                val=nrk_read(fd,&motion,2);   
    	        if(motion == 1)
	            {
                 //   printf("The value read from the PIR sensor is %u\r\n", motion);
                    pir_count++;
                }
                val=nrk_set_status(fd,SENSOR_SELECT,AUDIO_P2P);
                val=nrk_read(fd,&audio,2);   
        	    if(audio > 100)
        	    {
                   // printf("The value read from the microphone is %u\r\n", audio);
                    audio_count++;
                }
                
                nrk_time_get(&end_time);
                if(end_time.secs - start_time.secs > 1)
                {
                    nrk_led_toggle(GREEN_LED);
                    nrk_time_get(&start_time);
                    if(audio_count > 9 && pir_count > 9)
                    {
                        printf("Yeah Ambulance \r\n");
                        break;
                    }
                    audio_count = 0;
                    pir_count = 0;
                }
                if(to_flag!=0)
                {
                    if(bmac_rx_pkt_ready())
                    {   
                        localrx_buf = bmac_rx_pkt_get (&len, &rssi);
                        bmac_rx_pkt_release ();
                        printf("Received Flush : %s \r\n",localrx_buf);
                        if(localrx_buf[0] == 'C' && (localrx_buf[1] - '0') == MAC_ADDR)
                        {
                            break;
                        }
                    }
                }
            }
            nrk_led_clr(GREEN_LED);
            //Send back reply
            sprintf (tx_buf, "N%d", MAC_ADDR);
            printf("Sending reply  %s \r\n", tx_buf);
            val=bmac_tx_pkt(tx_buf, strlen(tx_buf)+1);    
            if(val != NRK_OK)
            {
                nrk_kprintf(PSTR("Could not Transmit!\r\n"));
            }

            nrk_led_set(GREEN_LED);
            nrk_led_set(RED_LED);
            nrk_led_set(BLUE_LED);
            nrk_led_set(ORANGE_LED);
        }
        else
        {
            nrk_time_get(&start_time);
            nrk_time_get(&end_time);
            while(end_time.secs-start_time.secs < 2)
            {
                nrk_time_get(&end_time);
            }

            //Sending Updated Rout info
            printf("Sending rout info  %s \r\n", local_buf);
            nrk_led_set (BLUE_LED);
            val=bmac_tx_pkt(local_buf, strlen(local_buf)+1);
            if(val != NRK_OK)
            {
        	    nrk_kprintf(PSTR("Could not Transmit!\r\n"));
            }
            
            if(to_flag == 0)
            {
                //Wait for reply
   NORMAL_FLOW: do
                {
                    if(!bmac_rx_pkt_ready())
                    {
           	            val = bmac_wait_until_rx_pkt ();
                    }
                    localrx_buf = bmac_rx_pkt_get (&len, &rssi);
                    printf("Received Reply : %s \r\n",localrx_buf);
                    bmac_rx_pkt_release ();
                
                }while(localrx_buf[0] != 'N' || (localrx_buf[1] - '0') != rx_node_id); 
            
                nrk_led_clr (BLUE_LED);
                //Sample sensors 
                nrk_time_get(&start_time);
                time_out = 0;
                negative_flag = 0;
                while (1)
                {
                    val=nrk_set_status(fd,SENSOR_SELECT,MOTION);
                    val=nrk_read(fd,&motion,2);   
        	        if(motion == 1)
    	            {
                        pir_count++;
                    }
                    val=nrk_set_status(fd,SENSOR_SELECT,AUDIO_P2P);
                    val=nrk_read(fd,&audio,2);   
            	    if(audio > 100)
            	    {
                        audio_count++;
                    }
                    
                    nrk_time_get(&end_time);
                    if(end_time.secs - start_time.secs > 1)
                    {
                        time_out++;
                        nrk_led_toggle(GREEN_LED);
                        nrk_time_get(&start_time);
                        if(audio_count > 9 && pir_count > 9)
                        {       
                            printf("Yeah Ambulance \r\n");
                            break;
                        }
                        audio_count = 0;
                        pir_count = 0;
                    }
    
                    if(time_out == 5)
                    {
                        negative_flag = 1;
                        nrk_led_set(RED_LED);
                        break;
                    }
                }
            
                if(negative_flag == 0)
                {
                    nrk_led_clr(GREEN_LED);
                    //Send back reply
                    sprintf (tx_buf, "N%d", MAC_ADDR);
                    printf("Sending reply  %s \r\n", tx_buf);
                    val=bmac_tx_pkt(tx_buf, strlen(tx_buf)+1);    
            	    if(val != NRK_OK)
                    {
                        nrk_kprintf(PSTR("Could not Transmit!\r\n"));
                    }
                }
                else
                {
                    nrk_time_get(&start_time);
                    nrk_time_get(&end_time);
                    while(end_time.secs-start_time.secs < 2)
                    {
                        nrk_time_get(&end_time);
                    }
                    local_buf[0]='T';
                    for(k = 1; routing_array[k]!='F';k++)
                    {
                        local_buf[k] = routing_array[k] + '0';
                    }
                    local_buf[k] = routing_array[k];
                    printf("Sending Time_out info  %s \r\n", local_buf);
                    val=bmac_tx_pkt(local_buf, strlen(local_buf)+1);
                    if(val != NRK_OK)
                    {
            	        nrk_kprintf(PSTR("Could not Transmit!\r\n"));
                    }
    
                    //Sample sensors 
                    nrk_time_get(&start_time);
                    while (1)
                    {
                        val=nrk_set_status(fd,SENSOR_SELECT,MOTION);
                        val=nrk_read(fd,&motion,2);   
        	            if(motion == 1)
    	                {
                             //   printf("The value read from the PIR sensor is %u\r\n", motion);
                            pir_count++;
                        }
                        val=nrk_set_status(fd,SENSOR_SELECT,AUDIO_P2P);
                        val=nrk_read(fd,&audio,2);   
                	    if(audio > 100)
                	    {
                           // printf("The value read from the microphone is %u\r\n", audio);
                            audio_count++;
                        }
                        
                        nrk_time_get(&end_time);
                        if(end_time.secs - start_time.secs > 1)
                        {
                            nrk_led_toggle(GREEN_LED);
                            nrk_time_get(&start_time);
                            if(audio_count > 9 && pir_count > 9)
                            {
                                printf("Yeah Ambulance \r\n");
                                break;
                            }
                            audio_count = 0;
                            pir_count = 0;
                        }
                    
                        if(bmac_rx_pkt_ready())
                        {      
                            localrx_buf = bmac_rx_pkt_get (&len, &rssi);
                            bmac_rx_pkt_release ();
                            printf("Received: %s \r\n",localrx_buf);
                            if(localrx_buf[0] == 'E' && (localrx_buf[1] - '0') == MAC_ADDR) 
                            {
                                nrk_led_clr(GREEN_LED);
                                nrk_led_clr(RED_LED);
                                nrk_led_clr(BLUE_LED);
                                nrk_led_set(ORANGE_LED);
                                goto NORMAL_FLOW;
                            }
                        }
                    }
                    
                    nrk_led_clr(GREEN_LED);
                    //Send back reply 
                    sprintf (tx_buf, "N%d", MAC_ADDR);
                    printf("Sending reply  %s \r\n", tx_buf);
                    val=bmac_tx_pkt(tx_buf, strlen(tx_buf)+1);    
            	    if(val != NRK_OK)
                    {
                        nrk_kprintf(PSTR("Could not Transmit!\r\n"));
                    }
                    
                    
                    nrk_time_get(&start_time);
                    nrk_time_get(&end_time);
                    while(end_time.secs-start_time.secs < 2)
                    {
                        nrk_time_get(&end_time);
                    }
                    sprintf (tx_buf, "C%d", rx_node_id);
                    printf("Sending flush  %s \r\n", tx_buf);
                    val=bmac_tx_pkt(tx_buf, strlen(tx_buf)+1);    
                	if(val != NRK_OK)
                    {
                         nrk_kprintf(PSTR("Could not Transmit!\r\n"));
                    }   
                }
            }
            else
            {
                //Sample sensors 
                nrk_time_get(&start_time);
                while (1)
                {
                    val=nrk_set_status(fd,SENSOR_SELECT,MOTION);
                    val=nrk_read(fd,&motion,2);   
        	        if(motion == 1)
    	            {
                       //   printf("The value read from the PIR sensor is %u\r\n", motion);
                        pir_count++;
                    }
                    val=nrk_set_status(fd,SENSOR_SELECT,AUDIO_P2P);
                    val=nrk_read(fd,&audio,2);   
                    if(audio > 100)
                    {
                      // printf("The value read from the microphone is %u\r\n", audio);
                         audio_count++;
                    }
                        
                    nrk_time_get(&end_time);
                    if(end_time.secs - start_time.secs > 1)
                    {
                        nrk_led_toggle(GREEN_LED);
                        nrk_time_get(&start_time);
                        if(audio_count > 9 && pir_count > 9)
                        {
                            printf("Yeah Ambulance \r\n");
                            break;
                        }
                        audio_count = 0;
                        pir_count = 0;
                    }
                    
                     
                    if(bmac_rx_pkt_ready())
                    {   
                        localrx_buf = bmac_rx_pkt_get (&len, &rssi);
                        bmac_rx_pkt_release ();
                        printf("Received Flush : %s \r\n",localrx_buf);
                        if(localrx_buf[0] == 'C' && (localrx_buf[1] - '0') == MAC_ADDR)
                        {
                            break;
                        }
                        if(localrx_buf[0] == 'E' && (localrx_buf[1] - '0') == MAC_ADDR)
                        {
                            sprintf (tx_buf, "E%d", tx_node_id);
                            printf("Sending get back to normal  %s \r\n", tx_buf);
                            val=bmac_tx_pkt(tx_buf, strlen(tx_buf)+1);    
                        	if(val != NRK_OK)
                            {
                                 nrk_kprintf(PSTR("Could not Transmit!\r\n"));
                            }
                            nrk_led_clr(GREEN_LED);
                            nrk_led_clr(RED_LED);
                            nrk_led_clr(BLUE_LED);
                            nrk_led_set(ORANGE_LED);
                            goto NORMAL_FLOW;
                        }
                        if(localrx_buf[0] == 'N' && (localrx_buf[1] - '0') == rx_node_id)
                        {
                            sprintf (tx_buf, "E%d", tx_node_id);
                            printf("Sending get back to normal  %s \r\n", tx_buf);
                            val=bmac_tx_pkt(tx_buf, strlen(tx_buf)+1);    
                        	if(val != NRK_OK)
                            {
                                 nrk_kprintf(PSTR("Could not Transmit!\r\n"));
                            }   
                        }
                    }
                }
                    
                nrk_led_clr(GREEN_LED);
                sprintf (tx_buf, "N%d", MAC_ADDR);
                printf("Sending reply  %s \r\n", tx_buf);
                val=bmac_tx_pkt(tx_buf, strlen(tx_buf)+1);    
            	if(val != NRK_OK)
                {
                    nrk_kprintf(PSTR("Could not Transmit!\r\n"));
                }
                    
                nrk_time_get(&start_time);
                nrk_time_get(&end_time);
                while(end_time.secs-start_time.secs < 2)
                {
                    nrk_time_get(&end_time);
                }
                sprintf (tx_buf, "C%d", rx_node_id);
                printf("Sending flush  %s \r\n", tx_buf);
                val=bmac_tx_pkt(tx_buf, strlen(tx_buf)+1);    
                if(val != NRK_OK)
                {
                    nrk_kprintf(PSTR("Could not Transmit!\r\n"));
                }   

            }
            nrk_led_set(GREEN_LED);
            nrk_led_set(RED_LED);
            nrk_led_set(BLUE_LED);
            nrk_led_set(ORANGE_LED);
        }
    }
}
void nrk_create_taskset ()
{
    NODE_TASK.task = node_task;
    nrk_task_set_stk( &NODE_TASK, node_task_stack, NRK_APP_STACKSIZE);
    NODE_TASK.prio = 2;
    NODE_TASK.FirstActivation = TRUE;
    NODE_TASK.Type = BASIC_TASK;
    NODE_TASK.SchType = PREEMPTIVE;
    NODE_TASK.period.secs = 1;
    NODE_TASK.period.nano_secs = 0;
    NODE_TASK.cpu_reserve.secs = 0;
    NODE_TASK.cpu_reserve.nano_secs = 0;
    NODE_TASK.offset.secs = 0;
    NODE_TASK.offset.nano_secs = 0;
    nrk_activate_task (&NODE_TASK);
    printf ("Create done\r\n");
}
void nrk_register_drivers()
{
    int8_t val;
    val=nrk_register_driver( &dev_manager_ff3_sensors,FIREFLY_3_SENSOR_BASIC);
    if(val==NRK_ERROR) 
        nrk_kprintf( PSTR("Failed to load my ADC driver\r\n") );
}
