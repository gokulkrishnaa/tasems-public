from flask import Flask, render_template, flash
from flask import request, json
from flask import make_response, current_app
from datetime import timedelta
import requests
import time
from functools import update_wrapper

from diijkstra import get_route
from dotGraph import dotPlot

app = Flask(__name__)
app.secret_key = 'tasems'

@app.route('/')
def tasems():
    response = render_template('index.html')
    return response

def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

@app.route('/api/patient/info/', methods = ['POST'])
@crossdomain(origin='*')
def patient_info():
    input_json = request.get_json(force=True)
    info = json.dumps(input_json, indent = 4)
    print "Got Request: ",info

    hospitalDict = {'gun-shot' : 'H1', 'heart-attack' :'H1', 'cyanide' : 'H2'}
    location = str(input_json['location'])
    injury = str(input_json['injury'])
    condition = str(input_json['condition'])
    hospital = hospitalDict.get(injury)
    config = input_json['config']
    config = config[-1:]
    response = {}

    if (hospital == 'H1'):
        hospitalURL = "http://wsn1.wv.cc.cmu.edu:1234/api/ems/route"
    else:
        hospitalURL = "http://wsn2.wv.cc.cmu.edu:1234/api/ems/route"

    # Call the shortest path algorithm and get the route
    route = get_route(hospital[-1:], location)
    print "ROUTE : ", route

    resp_to_hospital = {"route" : route, "condition" : condition, "injury" : injury}
    headers={}
    post = requests.post(hospitalURL, headers = headers, data = json.dumps(resp_to_hospital, indent = 4))
    print "Response to Hospital : ",json.dumps(resp_to_hospital, indent = 4)
    print post.json()

    if(condition == 'critical'):
        #print hospital
        response['success'] = 'True'
        response['hospital'] = hospital
        response['route'] = route

    else:
        #print 'Not Critical'
        response['success'] = 'True'
        response['hospital'] = hospital
        response['route'] = 'no'

    print "Response to EMS : ", json.dumps(response, indent = 4)
    return json.dumps(response)

@app.route('/api/Graph/', methods = ['POST'])
@crossdomain(origin='*')
def GraphInfo():
    val = request.get_json(force = True)
    val_json = json.dumps(val, indent = 4)

    gg = open("graph.json", "wb")
    gg.write(val_json)
    gg.close()

    time.sleep(2)
    dotPlot();

    response = {}
    response['success'] = 'True';
    flash("Updated the Graph Successfully")
    return json.dumps(response);

@app.route('/local/graph_html/', methods = ['POST'])
def localGraph():
    return render_template('newGraph.html')

if __name__ == '__main__':
    i = 0
    app.run(host='0.0.0.0', port=1234)
