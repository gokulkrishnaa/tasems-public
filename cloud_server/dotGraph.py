from graphviz import Digraph
import shutil
import time
import json


#if __name__ == '__main__':
def dotPlot():
    dot = Digraph(comment = "Node Visualization");
    #dot.engine = 'circo';
    #dot.engine = 'neato';
    dot.engine = 'dot';

    with open('graph.json') as jsonFile:
	print jsonFile
        nodeJSON = json.load(jsonFile)

    num_nodes = int(nodeJSON['vertices'])
    leaf = nodeJSON['leaf']
    nodes = []
    for i in range(0,num_nodes):
        nodes.append(i)
        if(str(i) != leaf):
            dot.node(str(i), 'NODE '+str(i), color = 'blue');
        else:
            dot.node(str(i), 'NODE '+str(i), color = 'blue', style = 'filled', fillcolor = 'green');

    edges = nodeJSON['edges']
    for val in edges:
        dot.edge(val[0], val[1], color = 'blue', label = ' ' + val[2]);

    dot.edge_attr.update(arrowhead = 'none')
    dot.format = 'png';
    dot.render('static/check', view = False);

    print dot
