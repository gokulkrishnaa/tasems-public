from collections import defaultdict, deque


class Graph(object):
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(list)
        self.distances = {}

    def add_node(self, value):
        self.nodes.add(value)

    def add_edge(self, from_node, to_node, distance):
        self.edges[from_node].append(to_node)
        self.edges[to_node].append(from_node)
        self.distances[(from_node, to_node)] = distance
        self.distances[(to_node, from_node)] = distance


def dijkstra(graph, initial):
    visited = {initial: 0}
    path = {}

    nodes = set(graph.nodes)

    while nodes:
        min_node = None
        for node in nodes:
            if node in visited:
                if min_node is None:
                    min_node = node
                elif visited[node] < visited[min_node]:
                    min_node = node
        if min_node is None:
            break

        nodes.remove(min_node)
        current_weight = visited[min_node]

        for edge in graph.edges[min_node]:
            try:
                weight = current_weight + graph.distances[(min_node, edge)]
            except:
                continue
            if edge not in visited or weight < visited[edge]:
                visited[edge] = weight
                path[edge] = min_node

    return visited, path


def shortest_path(graph, origin, destination):
    visited, paths = dijkstra(graph, origin)
    full_path = deque()
    _destination = paths[destination]

    while _destination != origin:
        full_path.appendleft(_destination)
        _destination = paths[_destination]

    full_path.appendleft(origin)
    full_path.append(destination)

    return visited[destination], list(full_path)

if __name__ == '__main__':
#def get_route(hospital): 
    graph = Graph()

    hospital = '1'
    if(hospital == '2'):
	hospital = '1'
    else:
	hospital = '0'

    #for node in range(0,8):
    for node in ['0', '1', '2', '3', '4', '5', '6', '7']:
        graph.add_node(node)
        #graph.add_node(str(node))
	#print str(node)

    graph.add_edge('0', '2', 1)
    graph.add_edge('2', '4', 2)
    graph.add_edge('2', '3', 3)
    graph.add_edge('3', '1', 1)
    graph.add_edge('3', '4', 2)
    graph.add_edge('3', '5', 3)
    graph.add_edge('2', '4', 2)
    graph.add_edge('4', '5', 2)
    graph.add_edge('5', '6', 2)
    graph.add_edge('6', '7', 2)

    path = shortest_path(graph, '7', hospital) # output: (25, ['A', 'B', 'D'])
    final_path = []
    final_path.append('H')
    
    for i in path[1]:
        final_path.append(i)
    final_path.append('F')
    path_to_cloud = ''.join(final_path)
    print path_to_cloud
    #return path_to_cloud
