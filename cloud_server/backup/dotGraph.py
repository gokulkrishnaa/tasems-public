from graphviz import Digraph
import shutil
import time

f = open('output.txt', 'r')

G  =  'G';
N1 = 'N1';
N2 = 'N2';
N3 = 'N3';

dot = Digraph(comment = "Node Visualization");
dot.engine = 'circo';

dot.node('0', 'GATEWAY', color = 'blue', shape = 'square', style = 'filled', fillcolor = 'blue');
dot.node('1', 'NODE 1', color = 'blue');
dot.node('2', 'NODE 2', color = 'blue');
dot.node('3', 'NODE 3', color = 'blue');

colors = ['green', 'red', 'orange', 'blue', 'black', 'red', 'orange', 'blue', 'black', 'red', 'orange', 'blue', 'black', 'red', 'orange', 'blue', 'black']
count = 0;

while True:
	line = f.readline()
	if 'routing list' in line:
   	 	s=line;
		a=s[s.find("[")+1:s.find("]")]
		length = len(a)

		if(length == 1):
			dot.edge(a[0], '0', color = colors[count]);

		elif(length == 2):
			dot.edge(a[1], a[0], color = colors[count]);
			dot.edge(a[0], '0', color = colors[count]);

		elif(length == 3):
			dot.edge(a[2], a[1], color = colors[count]);
			dot.edge(a[1], a[0], color = colors[count]);
			dot.edge(a[0], '0', color = colors[count]);

		dot.format = 'png';
		dot.render('graph/check', view = True);
		print dot
		count = count + 1

		curr_pos = f.tell()
		#time.sleep(3)
		f.seek(curr_pos)



