from collections import defaultdict, deque
import json

class Graph(object):
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(list)
        self.distances = {}

    def add_node(self, value):
        self.nodes.add(value)

    def add_edge(self, from_node, to_node, distance):
        self.edges[from_node].append(to_node)
        self.edges[to_node].append(from_node)
        self.distances[(from_node, to_node)] = distance
        self.distances[(to_node, from_node)] = distance


def dijkstra(graph, initial):
    visited = {initial: 0}
    path = {}

    nodes = set(graph.nodes)

    while nodes:
        min_node = None
        for node in nodes:
            if node in visited:
                if min_node is None:
                    min_node = node
                elif visited[node] < visited[min_node]:
                    min_node = node
        if min_node is None:
            break

        nodes.remove(min_node)
        current_weight = visited[min_node]

        for edge in graph.edges[min_node]:
            try:
                weight = current_weight + graph.distances[(min_node, edge)]
            except:
                continue
            if edge not in visited or weight < visited[edge]:
                visited[edge] = weight
                path[edge] = min_node

    return visited, path


def shortest_path(graph, origin, destination):
    visited, paths = dijkstra(graph, origin)
    full_path = deque()
    _destination = paths[destination]

    while _destination != origin:
        full_path.appendleft(_destination)
        _destination = paths[_destination]

    full_path.appendleft(origin)
    full_path.append(destination)

    return visited[destination], list(full_path)

#if __name__ == '__main__':
def get_route(hospital, location):
    graph = Graph()

    #hospital = '1'
    if(hospital == '2'):
	hospital = '1'
    else:
	hospital = '0'

    #location = 'Shadyside'
    if location == 'Shadyside':
        with open('graph_shady.json') as jsonFile:
	    nodeJSON = json.load(jsonFile)

    else:
        with open('graph.json') as jsonFile:
	    nodeJSON = json.load(jsonFile)

    num_nodes = int(nodeJSON['vertices'])
    for node in range(0,num_nodes):
        graph.add_node(str(node))

    leaf = nodeJSON['leaf']

    edges = nodeJSON['edges']
    for val in edges:
	graph.add_edge(val[0], val[1], int(val[2]))

    '''
    graph.add_edge('0', '2', 1)
    graph.add_edge('2', '4', 2)
    graph.add_edge('2', '3', 3)
    graph.add_edge('3', '1', 1)
    graph.add_edge('3', '4', 2)
    graph.add_edge('3', '5', 3)
    graph.add_edge('4', '5', 2)
    graph.add_edge('5', '6', 2)
    graph.add_edge('6', '7', 2)
    '''

    path = shortest_path(graph, leaf, hospital) # output: (25, ['A', 'B', 'D'])
    final_path = []
    final_path.append('F')

    for i in path[1]:
        final_path.append(i)
    final_path.append('1H')
    path_to_cloud = ''.join(final_path)
    #print path_to_cloud[::-1]
    return path_to_cloud[::-1]
