/******************************************************************************
*  Nano-RK, a real-time operating system for sensor networks.
*  Copyright (C) 2007, Real-Time and Multimedia Lab, Carnegie Mellon University
*  All rights reserved.
*
*  This is the Open Source Version of Nano-RK included as part of a Dual
*  Licensing Model. If you are unsure which license to use please refer to:
*  http://www.nanork.org/nano-RK/wiki/Licensing
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, version 2.0 of the License.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/
#include <nrk.h>
#include <include.h>
#include <ulib.h>
#include <stdio.h>
#include <avr/sleep.h>
#include <hal.h>
#include <bmac.h>
#include <nrk_error.h>
#define MAC_ADDR    0x0001	
nrk_task_type HOSPITAL_TASK;
NRK_STK hospital_task_stack[NRK_APP_STACKSIZE];
void hospital_task (void);
void nrk_create_taskset ();
uint8_t rx_buf[RF_MAX_PAYLOAD_SIZE];
uint8_t tx_buf[RF_MAX_PAYLOAD_SIZE];
//int8_t slaves[4] = {3,5,6,7};
int8_t slaves[5] = {2,4,5,6,7};
int8_t num_slaves = 5;
int8_t contact_node;
void nrk_register_drivers();
int main ()
{
  uint16_t div;
  nrk_setup_ports ();
  nrk_setup_uart (UART_BAUDRATE_115K2);
  nrk_init ();
  nrk_led_clr (0);
  nrk_led_clr (1);
  nrk_led_clr (2);
  nrk_led_clr (3);
  nrk_time_set (0, 0);
  bmac_task_config ();
  nrk_create_taskset ();
  nrk_start ();
  return 0;
}
void hospital_task ()
{
    uint8_t i, j, k, len, temp, count, flag, start,check;
    uint8_t tx_array[11]={0,0,0,0,0,0,0,0,0,0,0};
    int8_t rssi, val;
    uint8_t *local_buf = NULL, temp_buf[50];
    char option;
    
    start = 0;
    check = 1;
    k = 0;
    nrk_sig_t uart_rx_signal;
    nrk_time_t start_time, end_time;
    bmac_init (12);
    bmac_rx_pkt_set_buffer (rx_buf, RF_MAX_PAYLOAD_SIZE);

    nrk_led_set (RED_LED);
    printf ("hospital_task PID=%d\r\n", nrk_get_pid ());
    nrk_kprintf( PSTR("Waiting for request\r\n" ));
        
        do{
            if(nrk_uart_data_ready(NRK_DEFAULT_UART))
            {
    	    	option = getchar();
        		if((option == '#') && (start == 1))
    	    	{
    		    	temp_buf[k] = option;
	    		    k = 0;
        			printf("Read String : %s \r\n", temp_buf);
    	    		if(option == '#') check = 0;
    		    }
        		else if((option == 'H') || (start == 1))
    	    	{
        			start = 1;
    	    		temp_buf[k] = option;
    		    	k++;
        		}
            }
            else nrk_event_wait(SIG(uart_rx_signal));
        }while(check);
    
        contact_node = temp_buf[3] - '0';
        nrk_led_set (RED_LED);
        nrk_led_set (GREEN_LED);
        nrk_led_set (ORANGE_LED);
        nrk_led_set (BLUE_LED);
        
        nrk_time_get(&start_time);
        nrk_time_get(&end_time);
        while(end_time.secs-start_time.secs < 2)
        {
            nrk_time_get(&end_time);
        }

        //Sending Rout info
        printf("Sending route info  %s \r\n", temp_buf);
        nrk_led_set (BLUE_LED);
        val=bmac_tx_pkt(temp_buf, strlen(temp_buf)+1);
        if(val != NRK_OK)
        {
    	    nrk_kprintf(PSTR("Could not Transmit!\r\n"));
        }
        nrk_led_clr (BLUE_LED);
        nrk_led_clr (RED_LED);
        nrk_led_clr (ORANGE_LED);
        nrk_led_clr (GREEN_LED);
    
        //Waiting for Alert from contact node
        do
        {
            nrk_led_toggle (GREEN_LED);
            if(!bmac_rx_pkt_ready())
            {
       	        val = bmac_wait_until_rx_pkt ();
            }
            local_buf = bmac_rx_pkt_get (&len, &rssi);
            bmac_rx_pkt_release ();
            printf("Received Alert : %s \r\n",local_buf);
        }while(local_buf[0] != 'N' || (local_buf[1] - '0') != contact_node);
        nrk_led_clr (GREEN_LED);
        printf("Ambulance arriving shortly!\r\n");
        nrk_led_set (GREEN_LED);
        nrk_led_set (RED_LED);
        nrk_led_set (BLUE_LED);
        nrk_led_set (ORANGE_LED);
    
    while(1);
}
void nrk_create_taskset ()
{
  HOSPITAL_TASK.task = hospital_task;
  nrk_task_set_stk( &HOSPITAL_TASK, hospital_task_stack, NRK_APP_STACKSIZE);
  HOSPITAL_TASK.prio = 2;
  HOSPITAL_TASK.FirstActivation = TRUE;
  HOSPITAL_TASK.Type = BASIC_TASK;
  HOSPITAL_TASK.SchType = PREEMPTIVE;
  HOSPITAL_TASK.period.secs = 1;
  HOSPITAL_TASK.period.nano_secs = 0;
  HOSPITAL_TASK.cpu_reserve.secs = 0;
  HOSPITAL_TASK.cpu_reserve.nano_secs = 0;
  HOSPITAL_TASK.offset.secs = 0;
  HOSPITAL_TASK.offset.nano_secs = 0;
  nrk_activate_task (&HOSPITAL_TASK);
  printf ("Create done\r\n");
}
