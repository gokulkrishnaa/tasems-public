This is a repo for TASEMS

For details about this project, please visit : <br />
https://jithin18936.wixsite.com/tasems

The files in this repo are :
--------------------------

--------------------------------------------------------------------------------

Tasems_Android    ===>  The Android application for TASEMS <br />
cloud_server      ===>  A flask server to be run on a global domain <br />
hospital          ===>  The code for master node firefly that is <br />
                        connected via UART to hospital (raspberry Pi) <br />
hospital_server   ===>  A flask server to be run on a raspberry Pi <br />
node              ===>  The code for firefly nodes in the WSN  <br />

--------------------------------------------------------------------------------