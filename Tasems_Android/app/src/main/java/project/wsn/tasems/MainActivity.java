package project.wsn.tasems;

/*----------------------------------------------
* TASEMS Android application
* developed by Gokul Krishnaa Devaraju
* at Carnegie Mellon University
* as a part of Wireless Sensor Networks (18648)
----------------------------------------------*/

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends Activity {

    EditText edit_location, edit_hospital;
    Button button_location, button_submit;
    Spinner spinner_condition, spinner_injury, spinner_config;
    TextView text_hospital, text_config;
    ImageView image;

    RequestQueue queue;

    // URL of the cloud server and PORT it is listening on
    String bd_url = "http://bd-exp.andrew.cmu.edu:1234";

    ArrayAdapter<CharSequence> adapter_condition;
    String[] condition = {"critical", "non-critical"};
    ArrayAdapter<CharSequence> adapter_injury;

    // Fixed for three types of injuries but can be modified to include
    // more types by extending it to talk to a database in the cloud
    String[] injury = {"gun-shot", "heart-attack", "cyanide"};
    ArrayAdapter<CharSequence> adapter_config;

    // config is dummy for now but can be used to have specific WSN's
    String[] config = {"Config A", "Config B"};

    String request_string;
    JSONObject request_json;

    private BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;
    private boolean mScanning;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 3000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_location = (EditText) findViewById(R.id.edit_location);
        edit_location.setEnabled(false);
        edit_hospital = (EditText) findViewById(R.id.edit_hospital);
        edit_hospital.setVisibility(View.INVISIBLE);

        button_location = (Button) findViewById(R.id.button_location);
        button_submit   = (Button) findViewById(R.id.button_submit);

        text_config = ((TextView) findViewById(R.id.text_config));
        text_config.setVisibility(View.INVISIBLE);

        text_hospital = ((TextView) findViewById(R.id.text_hospital));
        text_hospital.setVisibility(View.INVISIBLE);

        image = (ImageView) findViewById(R.id.image_bg);
        image.setImageResource(R.mipmap.ic_background);

        BluetoothInit();
        spinnerInit();
        queue = Volley.newRequestQueue(this);

        button_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanLeDevice(true);
            }
        });

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                request_string = "{\"location\":\""+edit_location.getText()+
                        "\",\"condition\":\""+spinner_condition.getSelectedItem().toString()+
                        "\",\"injury\":\""+spinner_injury.getSelectedItem().toString()+
                        "\",\"hospital\":\""+edit_hospital.getText()+
                        "\",\"config\":\""+spinner_config.getSelectedItem().toString()+
                        "\"}";
                try {
                    request_json = new JSONObject(request_string);
                    JsonObjectRequest response_json = new JsonObjectRequest(Request.Method.POST,
                            bd_url+"/api/patient/info/",
                            request_json,
                            cloudResponse(),
                            null);

                    queue.add(response_json);
                    Log.d("JSON", request_json.toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public Response.Listener<JSONObject> cloudResponse() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("success").equals("True")){
                        Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "FAILED Try Again...", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi,
                             byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("device", device.getAddress());
                    if(device.getAddress().equals("0D:CB:50:35:C8:F1"))
                    {
                        edit_location.setText("Shadyside");
                        scanLeDevice(false);
                    }
                    else
                    {
                        edit_location.setText("Customized");
                        scanLeDevice(false);
                    }
                }
            });
        }
    };

    private void BluetoothInit(){

        // Use this check to determine whether BLE is supported on the device.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes Bluetooth adapter.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        mHandler = new Handler();

    }

    // Spinners for the UI to input the patient's condition and type of injury
    private void spinnerInit(){
        spinner_condition = (Spinner) findViewById(R.id.spinner_condition);
        adapter_condition = new ArrayAdapter<CharSequence>(this,android.R.layout.simple_spinner_item, condition);
        adapter_condition.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_condition.setAdapter(adapter_condition);

        spinner_injury = (Spinner) findViewById(R.id.spinner_injury);
        adapter_injury = new ArrayAdapter<CharSequence>(this,android.R.layout.simple_spinner_item, injury);
        adapter_injury.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_injury.setAdapter(adapter_injury);

        spinner_config = (Spinner) findViewById(R.id.spinner_config);
        adapter_config = new ArrayAdapter<CharSequence>(this,android.R.layout.simple_spinner_item, config);
        adapter_config.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_config.setAdapter(adapter_config);
        spinner_config.setVisibility(View.INVISIBLE);
    }

}
